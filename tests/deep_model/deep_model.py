import torch
import torch.nn as nn

from torchtrain import Settings, HParams, DeepModel, Training, tools

import os


class ConvNet(DeepModel):
    __scope__ = "model.conv_net"
    module_name = "tests/deep_model/deep_model.py"

    def __init__(self, **kwargs):
        super(ConvNet, self).__init__(**kwargs)

        with self.scope() as p:
            self.conv = nn.Sequential(
                nn.Conv2d(
                    in_channels=1,
                    out_channels=p.conv.filter,
                    kernel_size=p.conv.size,
                    stride=2,
                    padding=1,
                ),
                nn.Conv2d(
                    in_channels=p.conv.filter,
                    out_channels=p.conv.filter * 2,
                    kernel_size=p.conv.size,
                    stride=2,
                    padding=1,
                ),
                nn.Conv2d(
                    in_channels=p.conv.filter * 2,
                    out_channels=p.conv.filter * 4,
                    kernel_size=p.conv.size,
                    stride=2,
                ),
                nn.Flatten(),
                nn.Linear(p.conv.filter * 4 * 9, p.linear),
                nn.Linear(p.linear, 10),
                nn.LogSoftmax(dim=1),
            )

    def forward(self, x):
        return self.conv(x)


with Settings.default.scope(ConvNet.__scope__) as hparams:
    hparams.conv.filter = 16
    hparams.conv.size = 3
    hparams.linear = 1024

import git
import torch
from torch.utils.tensorboard import SummaryWriter

import unittest
import os
import shutil

from torchtrain import Settings, HParams


class TestSettings(unittest.TestCase):
    base_dir = "unittest/configs"
    try:
        os.makedirs(base_dir)
    except OSError:
        pass

    def test_expand(self):
        test = Settings(
            HParams(
                optimizer=HParams(lr=0.001, batch_size=256, epochs=100,),
                network=HParams(
                    layer1=HParams(kernel_size=64, batch_norm=True, activation="relu"),
                    layer2=HParams(kernel_size=128, batch_norm=True, activation="relu"),
                    layer3=HParams(
                        kernel_size=256, batch_norm=True, activation="sigmoid"
                    ),
                    residual=True,
                ),
            )
        )
        test.expand(True)  # enable auto expand
        # should work because auto expand is enable
        test.optimizer.new_hparams.test = 5
        test.expand(False)  # disable auto expand
        try:
            # should break because new_hparams2 doesn't exist
            test.optimizer.new_hparams2.test = 5
        except:
            pass
        else:
            assert False

    def test_expand_scope(self):
        test = Settings(
            HParams(
                optimizer=HParams(lr=0.001, batch_size=256, epochs=100,),
                network=HParams(
                    layer1=HParams(kernel_size=64, batch_norm=True, activation="relu"),
                    layer2=HParams(kernel_size=128, batch_norm=True, activation="relu"),
                    layer3=HParams(
                        kernel_size=256, batch_norm=True, activation="sigmoid"
                    ),
                    residual=True,
                ),
            )
        )
        print(test)
        # enable auto expand
        with test.scope("optimizer.new_hparams", expand=True) as p:
            # should work because auto expand is enable
            p.test = 5
        try:
            # should break because new_hparams2 doesn't exist
            with test.scope("optimizer.new_hparams2", expand=False) as p:
                p.test = 5
        except:
            pass
        else:
            assert False
        try:
            with test.scope("optimizer.new_hparams", expand=False) as p:
                # should break because test2 doesn't exist
                p.test2 = 5
        except:
            pass
        else:
            print("********************** error **********************")
            print(test)
            assert False

    def test_settings(self):

        Settings.set_default(
            HParams(
                optimizer=HParams(lr=0.001, batch_size=256, epochs=100,),
                network=HParams(
                    layer1=HParams(kernel_size=64, batch_norm=True, activation="relu"),
                    layer2=HParams(kernel_size=128, batch_norm=True, activation="relu"),
                    layer3=HParams(
                        kernel_size=256, batch_norm=True, activation="sigmoid"
                    ),
                    residual=True,
                ),
            )
        )
        config1 = Settings()
        config1.network.residual = False
        config1.test = True
        with config1.scope("loss") as loss:
            loss.weight = True
            loss.config = HParams(type="bce", decay=0.99)
        config1.save(os.path.join(self.base_dir, "config1.json"))

        config2 = Settings(HParams(optimizer=HParams(lr=0.1)))
        config2.save(os.path.join(self.base_dir, "config2.json"))

        config3 = Settings(
            HParams(
                optimizer=HParams(lr=0.1, epochs=50),
                network=HParams(layer2=HParams(activation="tanh")),
            )
        )
        config3.save(os.path.join(self.base_dir, "config3.json"))

        config1_load = Settings()
        config1_load.load(os.path.join(self.base_dir, "config1.json"))
        assert config1_load.optimizer.lr == 0.001
        assert config1_load.optimizer.batch_size == 256
        assert config1_load.optimizer.epochs == 100
        assert config1_load.network.layer1.kernel_size == 64
        assert config1_load.network.layer1.batch_norm == True
        assert config1_load.network.layer1.activation == "relu"
        assert config1_load.network.layer2.kernel_size == 128
        assert config1_load.network.layer2.batch_norm == True
        assert config1_load.network.layer2.activation == "relu"
        assert config1_load.network.layer3.kernel_size == 256
        assert config1_load.network.layer3.batch_norm == True
        assert config1_load.network.layer3.activation == "sigmoid"
        assert config1_load.network.residual == False
        assert config1_load.loss.weight == True
        assert config1_load.loss.config.decay == 0.99
        assert config1_load.loss.config.type == "bce"
        assert config1_load.test == True

        config2_load = Settings()
        config2_load.load(os.path.join(self.base_dir, "config2.json"))
        assert config2_load.optimizer.lr == 0.1
        assert config2_load.optimizer.batch_size == 256
        assert config2_load.optimizer.epochs == 100
        assert config2_load.network.layer1.kernel_size == 64
        assert config2_load.network.layer1.batch_norm == True
        assert config2_load.network.layer1.activation == "relu"
        assert config2_load.network.layer2.kernel_size == 128
        assert config2_load.network.layer2.batch_norm == True
        assert config2_load.network.layer2.activation == "relu"
        assert config2_load.network.layer3.kernel_size == 256
        assert config2_load.network.layer3.batch_norm == True
        assert config2_load.network.layer3.activation == "sigmoid"
        assert config2_load.network.residual == True

        config3_load = Settings()
        config3_load.load(os.path.join(self.base_dir, "config3.json"))
        assert config3_load.optimizer.lr == 0.1
        assert config3_load.optimizer.batch_size == 256
        assert config3_load.optimizer.epochs == 50
        assert config3_load.network.layer1.kernel_size == 64
        assert config3_load.network.layer1.batch_norm == True
        assert config3_load.network.layer1.activation == "relu"
        assert config3_load.network.layer2.kernel_size == 128
        assert config3_load.network.layer2.batch_norm == True
        assert config3_load.network.layer2.activation == "tanh"
        assert config3_load.network.layer3.kernel_size == 256
        assert config3_load.network.layer3.batch_norm == True
        assert config3_load.network.layer3.activation == "sigmoid"
        assert config3_load.network.residual == True

        str(config1_load)
        str(config2_load)
        str(config3_load)

        with config3_load.scope("network.layer2") as layer2:
            assert layer2.kernel_size == 128
            assert layer2.batch_norm == True
            assert layer2.activation == "tanh"
            with config2_load.scope("network") as nw:
                with config3_load.scope("optimizer") as optimizer:
                    assert optimizer.lr == 0.1
                    assert optimizer.batch_size == 256
                    assert optimizer.epochs == 50
                assert nw.layer1.kernel_size == 64
                assert nw.layer1.batch_norm == True
                assert nw.layer1.activation == "relu"
                assert nw.layer2.kernel_size == 128
                assert nw.layer2.batch_norm == True
                assert nw.layer2.activation == "relu"
                assert nw.layer3.kernel_size == 256
                assert nw.layer3.batch_norm == True
                assert nw.layer3.activation == "sigmoid"
                assert nw.residual == True
            assert layer2.kernel_size == 128
            assert layer2.batch_norm == True
            assert layer2.activation == "tanh"

        try:
            repo = git.Repo(search_parent_directories=True)
        except git.exc.InvalidGitRepositoryError:
            pass
        else:
            assert config3_load.commit == repo.head.object.hexsha

    def test_settings_tensorboard(self):
        directory = os.path.join(self.base_dir, "tensorboard")
        try:
            shutil.rmtree(directory)
        except OSError as e:
            pass
        writer = SummaryWriter(log_dir=directory)

        Settings.set_default(HParams())
        config = Settings()
        config.test.hparam1 = 1
        config.test.hparam2 = 2.0
        config.test.hparam3 = [3.0]
        config.test.hparam4 = "4.0"
        config.test6.hparam5 = "5.0"
        config.test.hparam6 = torch.tensor(6.0)
        config.test.hparam7 = torch.tensor([7, 7])
        writer.add_hparams(
            hparam_dict=config.to_tensorboard_hparams(), metric_dict={"metrics": 42}
        )


if __name__ == "__main__":
    unittest.main()

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.utils.tensorboard import SummaryWriter
import torchvision
import pandas as pd
import numpy as np

from torchtrain import (
    Settings,
    HParams,
    DeepModel,
    Training,
    TrainingBatch,
    Metric,
    tools,
)

from deep_model.deep_model import ConvNet

import unittest
import os
import shutil
import time
from typing import Any


class LossMetric(Metric):
    __name__ = "loss"

    def __init__(self, **kwargs):
        super(LossMetric, self).__init__(**kwargs)

    def init(self) -> None:
        self.loss_count = 0
        self.loss_sum = 0
        self.last_loss = 0

    def collect(self, batch_data: dict) -> None:
        assert "loss" in batch_data

        self.last_loss = batch_data["loss"]
        self.loss_count += 1
        self.loss_sum += self.last_loss

    def calculate(self) -> None:
        self.append(self.loss_sum / self.loss_count)

    def summarize(self) -> str:
        return f"loss: {self.get_last().item():.5f}"

    def benchmark(self) -> Any:
        return False

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        writer.add_scalar(
            os.path.join(tag_prefix, self.name), self.metrics[-1], global_step=step
        )

    def preview(self) -> Any:
        return f"loss: {self.last_loss.item():.5f}"


class F1Metric(Metric):
    __name__ = "f1"

    def __init__(self, **kwargs):
        super(F1Metric, self).__init__(**kwargs)

        self.num_class = kwargs.get("num_class", 1)

    def init(self) -> None:
        self.tp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.tn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fn = torch.zeros(self.num_class, dtype=int, device=self.device)

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "prediction" in batch_data and "ground_truth" in batch_data

        pred_class = batch_data["prediction"]
        pred = torch.zeros_like(pred_class, dtype=bool)
        i = torch.arange(0, pred.shape[0], dtype=int)
        pred[i, torch.argmax(pred_class, dim=1)] = True

        gt = torch.zeros_like(pred, dtype=bool)
        i = torch.arange(0, gt.shape[0], dtype=int)
        gt[i, batch_data["ground_truth"]] = True

        self.tp += (pred & gt).sum(dim=0)
        self.tn += ((~pred) & (~gt)).sum(dim=0)
        self.fp += (pred & (~gt)).sum(dim=0)
        self.fn += ((~pred) & gt).sum(dim=0)

    def calculate(self) -> None:
        precision = self.tp.float() / (self.tp + self.fp).float()
        recall = self.tp.float() / (self.tp + self.fn).float()
        f1 = 2 * (precision * recall) / (precision + recall)

        self.append(f1)

    def summarize(self) -> str:
        mean = self.get_last().mean()
        std = self.get_last().std()
        return f"f1: {mean*100:.2f}±{std*100:.2f}%"

    def benchmark(self) -> Any:
        return {"mean": self.get_last().mean(), "std": self.get_last().std()}

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        import matplotlib.pyplot as plt

        fig = plt.gcf()
        plt.bar(
            torch.arange(0, self.num_class, dtype=int),
            self.get_last().clone().detach().cpu().numpy(),
            label="f1",
        )
        plt.xlabel("classes")
        plt.ylabel("pourcentage")
        plt.legend()
        writer.add_figure(os.path.join(tag_prefix, self.name), fig, global_step=step)


class TestCustomTraining(Training):
    def __init__(self, **kwargs):
        super(TestCustomTraining, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"], F1Metric(num_class=10, device=self.device)
        )

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        self.epoch = self.hparams.epoch
        self.batch_size = self.hparams.batch_size

        self.model = ConvNet(**self.hparams.params)

        self.optimizer = [optim.Adam(self.model.parameters(), lr=self.hparams.lr)]
        self.loss = nn.NLLLoss()

        if train:
            if dataset_path is None:
                raise Exception("Invalid dataset path")
            self.train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    dataset_path,
                    train=True,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                num_workers=self.num_workers,
                batch_size=self.batch_size,
                shuffle=True,
            )

        if validation or test:
            test_validation_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    self.dataset_path,
                    train=False,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                num_workers=self.num_workers,
                batch_size=self.batch_size,
                shuffle=True,
            )
        if validation:
            self.validation_loader = test_validation_loader
        if test:
            self.test_loader = test_validation_loader

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer[0].zero_grad()

        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        l.backward()
        optimizer[0].step()

        return {"loss": l}

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        return {"loss": l, "prediction": y_pred, "ground_truth": y}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        [x, y] = data
        y_pred = model(x)
        l = loss(y_pred, y)

        return {"loss": l, "prediction": y_pred, "ground_truth": y}


class TestCustomTrainingBatch(TrainingBatch):
    def __init__(self, **kwargs):
        super(TestCustomTrainingBatch, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"], F1Metric(num_class=10, device=self.device)
        )

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        self.epoch = self.hparams.epoch
        self.batch_size = self.hparams.batch_size

        self.model = ConvNet(**self.hparams.params)

        self.optimizer = [optim.Adam(self.model.parameters(), lr=self.hparams.lr)]
        self.loss = nn.NLLLoss()

        if train:
            if dataset_path is None:
                raise Exception("Invalid dataset path")
            self.train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    dataset_path,
                    train=True,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                num_workers=self.num_workers,
                batch_size=self.batch_size,
                shuffle=True,
            )

        if validation or test:
            test_validation_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(
                    self.dataset_path,
                    train=False,
                    download=True,
                    transform=torchvision.transforms.Compose(
                        [
                            torchvision.transforms.ToTensor(),
                            torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                        ]
                    ),
                ),
                num_workers=self.num_workers,
                batch_size=self.batch_size,
                shuffle=True,
            )
        if validation:
            self.validation_loader = test_validation_loader
        if test:
            self.test_loader = test_validation_loader

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()

        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        l.backward()
        optimizer[0].step()

        return {"loss": l}

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        [x, y] = data
        y_pred = model(x)

        l = loss(y_pred, y)

        return {"loss": l, "prediction": y_pred, "ground_truth": y}

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        [x, y] = data
        y_pred = model(x)
        l = loss(y_pred, y)

        return {"loss": l, "prediction": y_pred, "ground_truth": y}


Settings.default.lr = 1e-4
Settings.default.epoch = 3
Settings.default.batch_size = 1024


class TestBrokenCustomTraining(Training):
    def __init__(self, **kwargs):
        super(TestBrokenCustomTraining, self).__init__(**kwargs)

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer[0].zero_grad()

        [x, y] = data
        y_pred = model(x)

        l = loss(y_prd, y)

        l.backward()
        optimizer[0].step()

        return {"loss": l}


def test_without_Training(dataset_path, epoch, batch_size):
    model = nn.Sequential(
        nn.Conv2d(1, 16, 3, stride=2, padding=1),
        nn.Conv2d(16, 32, 3, stride=2, padding=1),
        nn.Conv2d(32, 64, 3, stride=2),
        nn.Flatten(),
        nn.Linear(64 * 9, 128),
        nn.Linear(128, 10),
        nn.LogSoftmax(dim=1),
    )
    optimizer = optim.Adam(model.parameters(), lr=1e-3)
    loss = nn.NLLLoss()

    train_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(
            dataset_path,
            train=True,
            download=True,
            transform=torchvision.transforms.Compose(
                [
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                ]
            ),
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    test_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST(
            dataset_path,
            train=False,
            download=True,
            transform=torchvision.transforms.Compose(
                [
                    torchvision.transforms.ToTensor(),
                    torchvision.transforms.Normalize((0.1307,), (0.3081,)),
                ]
            ),
        ),
        batch_size=batch_size,
        shuffle=True,
    )
    for i in range(epoch):
        print(f"epoch {i+1}/{epoch}")
        for data in train_loader:
            optimizer.zero_grad()

            [x, y] = data
            y_pred = model(x)
            l = loss(y_pred, y)

            l.backward()
            optimizer.step()

        with torch.no_grad():
            for data in test_loader:
                [x, y] = data
                y_pred = model(x)
                l = loss(y_pred, y)

    tp, tn, fp, fn = 0, 0, 0, 0
    with torch.no_grad():
        for data in test_loader:
            [x, y] = data
            y_pred = model(x)
            l = loss(y_pred, y)

            N = y.shape[0]
            C = y_pred.shape[1]
            success = torch.argmax(y_pred, dim=1).eq(y).sum().item()
            tp += success
            tn += (C - 1) * success + (C - 2) * (N - success)
            fp += N - success
            fn += N - success

    accuracy = (tp + tn) / (tp + tn + fp + fn)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * (precision * recall) / (precision + recall)

    results = "[test] metrics: "
    results += f"accuracy: {accuracy*100:.2f}%  "
    results += f"precision: {precision*100:.2f}% "
    results += f"recall: {recall*100:.2f}% "
    results += f"f1: {f1*100:.2f}% "
    print(results)


class TestTraining(unittest.TestCase):
    base_dir = "unittest/trainings"
    try:
        os.makedirs(base_dir)
    except OSError:
        pass

    def test_training(self):
        directory = os.path.join(self.base_dir, "tensorboard")
        try:
            shutil.rmtree(directory)
        except OSError as e:
            pass

        start_custom = time.time()

        settings = Settings(
            hparams=HParams(
                lr=1e-4,
                epoch=3,
                batch_size=1024,
                model=HParams(conv_net=HParams(linear=128)),
            )
        )
        test = TestCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            hparams=settings,
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )

        test.run()

        end_custom = time.time()
        duration_custom = end_custom - start_custom
        print(f"Tolat time with Training: {tools.duration_display(duration_custom)}")

        start_classic = time.time()

        test_without_Training(
            dataset_path=os.path.join(self.base_dir, "mnist"), epoch=3, batch_size=1024
        )

        end_classic = time.time()
        duration_classic = end_classic - start_classic
        print(
            f"Tolat time without Training: {tools.duration_display(duration_classic)}"
        )

        diff = (duration_custom - duration_classic) / duration_classic
        print(f"Training take {diff*100:.1f}% more time")

        state = torch.load(os.path.join(self.base_dir, "checkpoint.pth"))
        state["hparams"]["epoch"] = 5
        torch.save(
            state, os.path.join(self.base_dir, "checkpoint.pth"),
        )

        print(f"Restart Training from the checkpoint (5 epochs in total)")
        test = TestCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            from_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )
        test.run()

    def test_training_unittest(self):
        settings = Settings(
            hparams=HParams(
                lr=1e-4,
                epoch=3,
                batch_size=1024,
                model=HParams(conv_net=HParams(linear=128)),
            )
        )
        test1 = TestCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            hparams=settings,
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )
        assert not test1.unittest()  # should work fine

        test2 = TestBrokenCustomTraining(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            hparams=settings,
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
        )
        assert test2.unittest()  # should break
        print("error well detected!")

    def test_training_batch(self):
        settings = Settings()
        test = TestCustomTrainingBatch(
            dataset_path=os.path.join(self.base_dir, "mnist"),
            hparams=settings,
            to_checkpoint=os.path.join(self.base_dir, "checkpoint.pth"),
            logs=os.path.join(self.base_dir, "tensorboard"),
            iter_train=100,
            iter_validate=10,
            iter_train_total=300,
            interval_backup=10,
        )

        test.run()


if __name__ == "__main__":
    unittest.main()

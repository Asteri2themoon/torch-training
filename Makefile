init:
	pip install -r requirements.txt

install: init
	sudo python setup.py install --force

reinstall: init
	sudo pip uninstall -y torch-train
	sudo python setup.py install --force

test: clear reinstall
#	py.test tests
	python tests/test_settings.py
	python tests/test_train.py

clear:
	rm -rf build
	rm -rf dist
	rm -rf torch_train.egg-info
	rm -rf tests/__pycache__
	rm -rf unittest
	rm -rf .pytest_cache

.PHONY: init test install
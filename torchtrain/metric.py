import torch

from torch.utils.tensorboard import SummaryWriter

from .warnings import NotImplementedWarning

import os
import warnings
import copy
from typing import Any


class Metric:
    __name__ = "metric"

    @property
    def name(self):
        return self.__name__

    def __init__(self, **kwargs):
        self.metrics = torch.tensor([])

        self.device = kwargs.get("device", "cpu")

    def append(self, vector: torch.Tensor):
        assert type(vector) == torch.Tensor

        flatten = vector.view(1, -1)

        if self.metrics.shape[0] == 0:
            self.metrics = flatten
        else:
            assert flatten.shape[1] == self.metrics.shape[1]
            self.metrics = torch.cat([self.metrics, flatten], dim=0)

    def get_last(self) -> torch.Tensor:
        if self.metrics.shape[0] > 0:
            return self.metrics[-1]
        return None

    def init(self) -> None:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.init is not implemented",
            NotImplementedWarning,
        )

    def collect(self, batch_data: dict) -> None:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.collect is not implemented",
            NotImplementedWarning,
        )

    def calculate(self) -> None:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.calculate is not implemented",
            NotImplementedWarning,
        )

    def summarize(self) -> str:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.summarize is not implemented",
            NotImplementedWarning,
        )

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.add_tensorboard is not implemented",
            NotImplementedWarning,
        )

    def benchmark(self) -> Any:
        warnings.warn(
            f"Warning: {self.__class__.__name__}.benchmark is not implemented",
            NotImplementedWarning,
        )
        return False

    def preview(self) -> Any:
        return False

    def save(self) -> torch.Tensor:
        return self.metrics.clone().detach()

    def load(self, metrics: torch.Tensor):
        assert type(metrics) == torch.Tensor

        self.metrics = metrics.clone().detach()

    def reset(self, metrics: torch.Tensor):
        self.metrics = torch.tensor([])


class Metrics:
    metrics_subset = ["train", "validate", "test"]

    def __init__(self):
        self.metrics = {key: {} for key in self.metrics_subset}

    def add_metrics(self, subset: Any, metric: Metric) -> None:
        assert type(subset) == str or type(subset) == list

        def add_metric_subset(subset, metric):
            assert subset in self.metrics_subset
            assert metric.name not in self.metrics[subset]

            self.metrics[subset][metric.name] = copy.deepcopy(metric)

        if type(subset) == str:
            add_metric_subset(subset, metric)
        elif type(subset) == list:
            for s in subset:
                assert type(s) == str
                add_metric_subset(s, metric)
        else:
            raise Exception("Error, subset must be a str or a list of str")

    def remove_metrics(self, subset: str, metric: Metric) -> None:
        assert subset in self.metrics_subset
        assert metric.name in self.metrics[subset]

        del self.metrics[subset][metric.name]

    def init(self, subset: str) -> None:
        assert subset in self.metrics_subset

        for key in self.metrics[subset].keys():
            self.metrics[subset][key].init()

    def collect(self, subset: str, batch_data: dict) -> None:
        assert subset in self.metrics_subset

        for key in self.metrics[subset].keys():
            self.metrics[subset][key].collect(batch_data)

    def calculate(self, subset: str) -> None:
        assert subset in self.metrics_subset

        for key in self.metrics[subset].keys():
            self.metrics[subset][key].calculate()

    def summarize(self, subset: str) -> str:
        assert subset in self.metrics_subset

        return " ".join([value.summarize() for value in self.metrics[subset].values()])

    def preview(self, subset: str) -> str:
        assert subset in self.metrics_subset

        preview = [value.preview() for value in self.metrics[subset].values()]

        def filterNoPreview(preview):
            return preview != False

        return " ".join(filter(filterNoPreview, preview))

    def add_tensorboard(
        self, subset: str, writer: SummaryWriter, step: int = None
    ) -> None:
        assert subset in self.metrics_subset
        assert type(writer) == SummaryWriter

        for value in self.metrics[subset].values():
            value.add_tensorboard(writer, step=step, tag_prefix=f"/Metrics/{subset}")

    def to_dict(self) -> dict:
        tmp = {key: {} for key in self.metrics_subset}

        for subset in self.metrics_subset:
            for metric in self.metrics[subset].keys():
                tmp[subset][metric] = self.metrics[subset][metric].save()
        return tmp

    def to_json(self) -> dict:
        tmp = {key: {} for key in self.metrics_subset}

        for subset in self.metrics_subset:
            for metric in self.metrics[subset].keys():
                tmp[subset][metric] = self.metrics[subset][metric].save().tolist()
        return tmp

    def from_dict(self, dct: dict) -> None:
        assert type(dct) == dict

        for subset in self.metrics_subset:
            if subset in dct:
                for metric in self.metrics[subset].keys():
                    if metric in dct[subset]:
                        self.metrics[subset][metric].load(dct[subset][metric])
                    else:
                        self.metrics[subset][metric].reset()

    def benchmark(self) -> dict:
        res = {}
        for metric in self.metrics["test"].values():
            b = metric.benchmark()
            if b != False:
                for k, v in b.items():
                    res["benchmark/" + metric.name + "/" + k] = v

        return res

    def save(self, file_name: str) -> None:
        assert type(file_name) == str

        torch.save(
            self.to_dict(), file_name,
        )

    def load(self, file_name: str) -> None:
        assert type(file_name) == str

        checkpoint = torch.load(file_name, map_location="cpu")
        self.from_dict(checkpoint)

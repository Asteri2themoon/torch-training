import torch
import torch.nn as nn
import pygit2

from .settings import Settings, HParams
from .warnings import (
    GitRepoNotFoundWarning,
    CommitDontMatchWarning,
    ModuleDontMatchWarning,
    ClassDontMatchWarning,
    NotImplementedWarning,
)

import os, sys, inspect
import math
import warnings
from typing import Type, Any
import importlib
import traceback


class DeepModel(nn.Module):
    __scope__ = "model.deepmodel"
    module_name = None

    def __init__(self, **kwargs):
        super(DeepModel, self).__init__()

        self.commit, self.remote_url, _ = DeepModel.get_current_git_state()

        self.hparams = Settings()
        with self.scope() as p:
            p.merge(HParams(**kwargs))

    @staticmethod
    def execution_path(filename, git=True):
        if filename == "__main__":
            raise Exception("Error: you're class is in __main__ file")

        frame = sys._getframe(1)
        if hasattr(frame, "_frame"):
            frame = frame._frame
        return os.path.join(os.path.dirname(inspect.getfile(frame)), filename)

    def to_dict(self):
        base = {
            "class": self.__class__.__name__,
            "hparams": self.hparams.to_dict(),
            "parameters": self.state_dict(),
        }
        if self.commit is not None:
            base["commit"] = self.commit
        if self.module_name is not None:
            base["remote_url"] = self.remote_url
        if self.remote_url is not None:
            base["module_name"] = self.module_name
        return base

    def from_dict(self, dct: dict, device: str = "cpu", strict: bool = True):
        settings = Settings()
        settings.from_dict(dct["hparams"])

        if "commit" in dct:
            self.commit = dct["commit"]
        if "remote_url" in dct:
            self.remote_url = dct["remote_url"]
        if "module_name" in dct:
            self.module_name = dct["module_name"]

        if self.commit != dct["commit"]:
            warnings.warn(
                "Warning: commit don't match", CommitDontMatchWarning,
            )
        if self.module_name != dct["module_name"]:
            warnings.warn(
                "Warning: module name don't match", ModuleDontMatchWarning,
            )
        if self.__class__.__name__ != dct["class"]:
            warnings.warn(
                "Warning: class name don't match", ClassDontMatchWarning,
            )

        self.load_state_dict(dct["parameters"], strict=strict)
        self.to(device)
        self.eval()

    def save(self, file_name):
        torch.save(
            self.to_dict(), file_name,
        )

    def load(self, file_name):
        checkpoint = torch.load(file_name, map_location="cpu")
        self.from_dict(checkpoint)

    @staticmethod
    def load_all(
        file_name: str, tmp_path: str = "/var/tmp/torch-train", verbose=True
    ) -> Any:
        checkpoint = torch.load(file_name, map_location="cpu")

        # checking current git
        _, remote_url, repo_path = DeepModel.get_current_git_state()

        # download git repo if needed
        if remote_url is None or remote_url != checkpoint.get("remote_url", None):
            repo_name = os.path.split(checkpoint["remote_url"])[1]
            path = os.path.splitext(repo_name)[0]
            repo_path = os.path.join(tmp_path, path)
            if pygit2.discover_repository(repo_path) is None:
                if verbose:
                    print("cloning repo", checkpoint["remote_url"])
                pygit2.clone_repository(checkpoint["remote_url"], repo_path)
                if verbose:
                    print("cloned!")
        else:
            repo_path = os.path.join(repo_path, "..")

        # get git Repository
        repo = pygit2.Repository(repo_path)
        current_head = str(repo.head.target)

        # stash commit if needed
        try:
            repo.stash(
                pygit2.Signature("torch-vision", "torch-vision"),
                "torch-train stashing to get the good version of the commit",
            )
            if verbose:
                print(f"stash changes")
        except KeyError:
            stash = False
        else:
            stash = True

        # go back to commit if needed
        if checkpoint["commit"] != current_head:
            if verbose:
                print(f"go back to commit {checkpoint['commit']}")
            rev = repo.revparse_single(checkpoint["commit"])
            repo.checkout_tree(rev)

        # find class
        try:
            spec = importlib.util.spec_from_file_location(
                "", os.path.join(repo_path, checkpoint["module_name"])
            )
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)

            class_ = getattr(module, checkpoint["class"])

            loaded_module = class_()
            loaded_module.from_dict(checkpoint)
        except:
            if verbose:
                traceback.print_exc()
                print("Fail to load checkpoint :-(")
            loaded_module = None

        # restore commit if needed
        if checkpoint["commit"] != current_head:
            if verbose:
                print(f"restore commit {current_head}")
            rev = repo.revparse_single(current_head)
            repo.reset(rev.id, pygit2.GIT_RESET_HARD)

        # pop stash if needed
        if stash:
            if verbose:
                print(f"pop stash")
            repo.stash_pop()

        return loaded_module

    @staticmethod
    def get_current_git_state(current_dir=os.getcwd()):
        commit = None
        remote_url = None
        repository_path = pygit2.discover_repository(current_dir)
        if repository_path != None:
            repo = pygit2.Repository(repository_path)
            commit = str(repo.head.target)
            for o in repo.remotes:
                remote_url = str(o.url)
                break
        else:
            warnings.warn(
                "Warning: git Repo not found, can't log commit number",
                GitRepoNotFoundWarning,
            )

        return commit, remote_url, repository_path

    @staticmethod
    def info(file_name, verbose=True):
        checkpoint = torch.load(file_name, map_location="cpu")
        print("module name:", checkpoint["module"])
        print("class name:", checkpoint["class"])
        print("project commit:", checkpoint["commit"])
        print("hyperparameters:", checkpoint["hparams"])
        return checkpoint

    def scope(self):
        return self.hparams.scope(self.__scope__)

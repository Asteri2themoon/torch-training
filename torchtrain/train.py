import torch
from torch.utils.tensorboard import SummaryWriter
import pandas as pd
import numpy as np

from .settings import Settings
from .model import DeepModel
from .metric import Metric, Metrics
from .warnings import (
    GitRepoNotFoundWarning,
    CommitDontMatchWarning,
    ModuleDontMatchWarning,
    ClassDontMatchWarning,
    NotImplementedWarning,
)
from . import tprint
from . import tools


import time
import warnings
import copy
import traceback
import pdb
import json
import os


class Training:
    __unittest_break__ = 4

    def __init__(
        self,
        dataset_path: str = None,
        from_checkpoint: str = None,
        to_checkpoint: str = None,
        device: str = "cpu",
        hparams: Settings = Settings(),
        logs: str = None,
        num_workers: int = 4,
        metrics_output: str = None,
        exec_train: bool = True,
        exec_validation: bool = True,
        exec_test: bool = True,
        **kwargs,
    ):
        self.hparams = hparams
        self.current_epoch = 0
        self.batch_size = 1

        self.from_checkpoint = from_checkpoint
        self.to_checkpoint = to_checkpoint
        self.metrics_output = metrics_output

        if self.from_checkpoint is not None:
            state = torch.load(self.from_checkpoint)
            self.hparams.from_dict(state["hparams"])
            self.current_epoch = state["current_epoch"] + 1
            self.batch_size = state["batch_size"]

        self.dataset_path = dataset_path
        self.num_workers = num_workers
        self.train_loader = None
        self.validation_loader = None
        self.test_loader = None

        self.device = device
        self.epoch = 0

        self.optimizer = None
        self.model = None
        self.loss = None

        if logs is not None:
            self.writer = SummaryWriter(log_dir=logs)
        else:
            self.writer = None

        self.metrics = Metrics()

        self.exec_train = exec_train
        self.exec_validation = exec_validation
        self.exec_test = exec_test

    def save_checkpoint(self, file_name: str) -> None:
        torch.save(
            {
                "model": self.model.to_dict(),
                "current_epoch": self.current_epoch,
                "batch_size": self.batch_size,
                "hparams": self.hparams.to_dict(),
                "optimizer": [o.state_dict() for o in self.optimizer],
                "metrics": self.metrics.to_dict(),
            },
            file_name,
        )

    def detach_dict(self, dct: dict) -> None:
        for k, v in dct.items():
            if type(v) == dict:
                self.detach_dict(dct[k])
            elif type(v) == torch.Tensor:
                dct[k] = v.clone().detach()

    @staticmethod
    def __flatten_dict__(dct: dict, root: str = "") -> dict:
        result = {}
        for k, v in dct.items():
            new_k = root + "/" + k if len(root) != 0 else k
            if type(v) == dict:
                tmp = Settings.__flatten_dict__(v, new_k)
                for k2, v2 in tmp.items():
                    result[k2] = v2
            else:
                result[new_k] = copy.deepcopy(v)
        return result

    @staticmethod
    def from_df(df: pd.DataFrame, name: str):
        values = df[name].values
        if type(values[0]) == list:
            return np.concatenate(values).reshape((-1, len(values[0])))
        else:
            return np.array(values)

    def tensorboard_add_scalar(
        self, metrics: dict, root: str = "", step: int = None
    ) -> None:
        if self.writer is not None:
            flatten_metrics = Training.__flatten_dict__(metrics, root=root)
            for k, v in flatten_metrics.items():
                name = k if len(root) == 0 else root + "/" + k
                self.writer.add_scalar(name, v, global_step=step)

    # run training
    def run(self, unittest: bool = False):
        state_optim = None
        state_model = None
        if self.from_checkpoint is not None:
            state = torch.load(self.from_checkpoint, map_location=self.device)
            state_optim = state["optimizer"]
            state_model = state["model"]
            self.metrics.from_dict(state["metrics"])

        # load dataset
        self.init(
            self.hparams,
            self.dataset_path,
            train=self.exec_train,
            validation=self.exec_validation,
            test=self.exec_test,
        )

        if state_model is not None:
            self.model.from_dict(state_model, device=self.device)

        if state_optim is not None:
            for i, state in enumerate(state_optim):
                self.optimizer[i].load_state_dict(state)

        it = range(self.current_epoch, self.epoch)
        if unittest:
            it = range(3)

        self.start = time.time()
        for self.current_epoch in it:
            self.model = self.model.to(self.device)
            tprint.print_line(
                f"epoch {self.current_epoch+1}/{self.epoch}", skip_line=True
            )

            # train
            if self.exec_train:
                self.metrics.init("train")
                self.train(self.train_loader, self.current_epoch, unittest=unittest)
                self.metrics.calculate("train")
                tprint.print_line(
                    f"[train] {self.metrics.summarize('train')}", skip_line=True
                )
                if self.writer is not None:
                    self.metrics.add_tensorboard(
                        subset="train", writer=self.writer, step=self.current_epoch
                    )

            # validation
            if self.exec_validation:
                self.metrics.init("validate")
                self.validate(
                    self.validation_loader, self.current_epoch, unittest=unittest
                )
                self.metrics.calculate("validate")
                tprint.print_line(
                    f"[valid] {self.metrics.summarize('validate')}", skip_line=True
                )
                if self.writer is not None:
                    self.metrics.add_tensorboard(
                        subset="validate", writer=self.writer, step=self.current_epoch
                    )

            # backup
            if self.to_checkpoint is not None:
                self.save_checkpoint(self.to_checkpoint)
            if self.metrics_output is not None:
                with open(self.metrics_output, "w") as json_file:
                    json.dump(
                        self.metrics.to_json(), json_file, indent=4, sort_keys=True
                    )

        # test
        if self.exec_test:
            self.metrics.init("test")
            self.test(self.test_loader, unittest=unittest)
            self.metrics.calculate("test")
            tprint.print_line(
                f"[test] {self.metrics.summarize('test')}", skip_line=True
            )
            if self.writer is not None:
                self.metrics.add_tensorboard(
                    subset="test", writer=self.writer, step=self.current_epoch
                )
                self.writer.add_hparams(
                    hparam_dict=self.hparams.to_tensorboard_hparams(),
                    metric_dict=self.metrics.benchmark(),
                )
            if self.to_checkpoint is not None:
                self.save_checkpoint(self.to_checkpoint)
            if self.metrics_output is not None:
                with open(self.metrics_output, "w") as json_file:
                    json.dump(
                        self.metrics.to_json(), json_file, indent=4, sort_keys=True
                    )

    # check is everything work fine
    def unittest(self, verbose=True):
        try:
            self.run(unittest=True)
        except:
            if verbose:
                print(traceback.format_exc())
            return True
        else:
            return False

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        state_optim: dict = None,
        state_model: dict = None,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.init is not implemented",
            NotImplementedWarning,
        )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.train_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # validate on one epoch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.validate_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # test on one epoch
    def test_batch(self, data, model, optimizer, loss, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.test_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # early stop mecanism
    def early_stop(self, metrics: dict):
        return False

    def to_recursive(self, obj, device):
        if type(obj) == list:
            return [self.to_recursive(e, device) for e in obj]
        elif type(obj) == dict:
            return {k: self.to_recursive(v, device) for k, v in obj.items()}
        elif type(obj) == torch.Tensor:
            return obj.to(device)
        return copy.deepcopy(obj)

    def train(self, dataset, epoch: int, unittest: bool = False) -> list:
        self.model.train()

        epoch_start = time.time()
        batch_count = len(dataset)
        for j, batch in enumerate(dataset):
            # train
            batch = self.to_recursive(batch, self.device)

            log = self.train_batch(
                data=batch,
                model=self.model,
                optimizer=self.optimizer,
                loss=self.loss,
                epoch=epoch,
                batch=j,
            )
            self.detach_dict(log)
            self.metrics.collect("train", log)

            # display
            if (j % 10) == 0 or (j + 1) == batch_count:
                progress = (j + 1) / batch_count
                display_log = "[train] {} {} (remaining time: {} sec)".format(
                    tprint.bar(progress, width=40),
                    self.metrics.preview("train"),
                    tools.remaining_time(epoch_start, time.time(), progress),
                )
                tprint.print_line(display_log)

            if j > self.__unittest_break__ and unittest:
                break

    def validate(self, dataset, epoch: int, unittest: bool = False) -> list:
        self.model.eval()

        epoch_start = time.time()
        self.metrics.init("validate")
        with torch.no_grad():
            batch_count = len(dataset)
            for j, batch in enumerate(dataset):
                # eval
                batch = self.to_recursive(batch, self.device)
                log = self.validate_batch(
                    data=batch,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    epoch=epoch,
                    batch=j,
                )
                self.detach_dict(log)
                self.metrics.collect("validate", log)

                # display
                if (j % 10) == 0 or (j + 1) == batch_count:
                    progress = (j + 1) / batch_count
                    display_log = "[valid] {} {} (remaining time: {} sec)".format(
                        tprint.bar(progress, width=40),
                        self.metrics.preview("validate"),
                        tools.remaining_time(epoch_start, time.time(), progress),
                    )
                    tprint.print_line(display_log)

                if j > self.__unittest_break__ and unittest:
                    break

    def test(self, dataset, unittest: bool = False) -> list:
        self.model.eval()

        epoch_start = time.time()
        self.metrics.init("validate")
        with torch.no_grad():
            batch_count = len(dataset)
            for j, batch in enumerate(dataset):
                # eval
                batch = self.to_recursive(batch, self.device)
                log = self.test_batch(
                    data=batch,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    batch=j,
                )
                self.detach_dict(log)
                self.metrics.collect("test", log)

                # display
                if (j % 10) == 0 or (j + 1) == batch_count:
                    progress = (j + 1) / batch_count
                    display_log = "[test] {} {} (remaining time: {} sec)".format(
                        tprint.bar(progress, width=40),
                        self.metrics.preview("test"),
                        tools.remaining_time(epoch_start, time.time(), progress),
                    )
                    tprint.print_line(display_log)

                if j > self.__unittest_break__ and unittest:
                    break


class TrainingBatch(Training):
    __unittest_break__ = 4

    def __init__(
        self,
        iter_train: int = 1000,
        iter_validate: int = 100,
        iter_train_total: int = 100000,
        interval_backup: float = 600,  # sed
        **kwargs,
    ):
        super(TrainingBatch, self).__init__(**kwargs)

        self.iter_train = iter_train
        self.iter_validate = iter_validate
        self.iter_train_total = iter_train_total
        self.interval_backup = interval_backup

    # run training
    def run(self, unittest: bool = False):
        state_optim = None
        state_model = None
        if self.from_checkpoint is not None:
            state = torch.load(self.from_checkpoint, map_location=self.device)
            state_optim = state["optimizer"]
            state_model = state["model"]
            self.metrics.from_dict(state["metrics"])

        # load dataset
        self.init(
            self.hparams,
            self.dataset_path,
            train=self.exec_train,
            validation=self.exec_validation,
            test=self.exec_test,
        )

        if state_model is not None:
            self.model.from_dict(state_model, device=self.device)

        if state_optim is not None:
            for i, state in enumerate(state_optim):
                self.optimizer[i].load_state_dict(state)

        sampler_train = tools.ContinuousSampler(self.train_loader)
        sampler_validation = tools.ContinuousSampler(self.validation_loader)

        if unittest:
            self.iter_train_total = 3 * self.iter_train
        
        self.model = self.model.to(self.device)

        self.start = time.time()
        trigger_backup = self.start + self.interval_backup
        while sampler_train.count < self.iter_train_total:
            # train
            self.metrics.init("train")
            self.train(sampler_train, unittest=unittest)
            self.metrics.calculate("train")
            tprint.print_line(
                f"[train] {self.metrics.summarize('train')}", skip_line=True
            )
            if self.writer is not None:
                self.metrics.add_tensorboard(
                    subset="train", writer=self.writer, step=sampler_train.count
                )

            # validate
            self.metrics.init("validate")
            self.validate(sampler_validation, unittest=unittest)
            self.metrics.calculate("validate")
            tprint.print_line(
                f"[valid] {self.metrics.summarize('validate')}", skip_line=True
            )
            if self.writer is not None:
                self.metrics.add_tensorboard(
                    subset="validate", writer=self.writer, step=sampler_train.count
                )

            if time.time() > trigger_backup:
                # backup
                if self.to_checkpoint is not None:
                    self.save_checkpoint(self.to_checkpoint)
                if self.metrics_output is not None:
                    with open(self.metrics_output, "w") as json_file:
                        json.dump(
                            self.metrics.to_json(), json_file, indent=4, sort_keys=True
                        )
                trigger_backup += self.interval_backup

        # test
        if self.exec_test:
            self.metrics.init("test")
            self.test(self.test_loader, unittest=unittest)
            self.metrics.calculate("test")
            tprint.print_line(
                f"[test] {self.metrics.summarize('test')}", skip_line=True
            )
            if self.writer is not None:
                self.metrics.add_tensorboard(
                    subset="test", writer=self.writer, step=self.current_epoch
                )
                self.writer.add_hparams(
                    hparam_dict=self.hparams.to_tensorboard_hparams(),
                    metric_dict=self.metrics.benchmark(),
                )
            if self.to_checkpoint is not None:
                self.save_checkpoint(self.to_checkpoint)
            if self.metrics_output is not None:
                with open(self.metrics_output, "w") as json_file:
                    json.dump(
                        self.metrics.to_json(), json_file, indent=4, sort_keys=True
                    )

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.train_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # validate on one epoch
    def validate_batch(self, data, model, optimizer, loss, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.validate_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    # test on one epoch
    def test_batch(self, data, model, optimizer, loss, batch):
        warnings.warn(
            f"Warning: {self.__class__.__name__}.test_batch is not implemented",
            NotImplementedWarning,
        )
        return {}

    def train(self, sampler, unittest: bool = False) -> list:
        self.model.train()

        epoch_start = time.time()
        b0 = -1
        for batch, data in sampler.iter_for(self.iter_train):
            if b0 == -1:
                b0 = batch
            # train
            data = self.to_recursive(data, self.device)

            log = self.train_batch(
                data=data,
                model=self.model,
                optimizer=self.optimizer,
                loss=self.loss,
                batch=batch,
            )
            self.detach_dict(log)
            self.metrics.collect("train", log)

            progress = (batch + 1 - b0) / self.iter_train
            display_log = "[train] {} {} (remaining time: {} sec)".format(
                tprint.bar(progress, width=40),
                self.metrics.preview("train"),
                tools.remaining_time(epoch_start, time.time(), progress),
            )
            tprint.print_line(display_log)

            if batch > self.__unittest_break__ and unittest:
                break

    def validate(self, sampler, unittest: bool = False) -> list:
        self.model.eval()

        epoch_start = time.time()
        self.metrics.init("validate")
        with torch.no_grad():
            b0 = -1
            for batch, data in sampler.iter_for(self.iter_validate):
                if b0 == -1:
                    b0 = batch
                # eval
                data = self.to_recursive(data, self.device)
                log = self.validate_batch(
                    data=data,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    batch=batch,
                )
                self.detach_dict(log)
                self.metrics.collect("validate", log)

                # display
                progress = (batch + 1 - b0) / self.iter_validate
                display_log = "[valid] {} {} (remaining time: {} sec)".format(
                    tprint.bar(progress, width=40),
                    self.metrics.preview("validate"),
                    tools.remaining_time(epoch_start, time.time(), progress),
                )
                tprint.print_line(display_log)

                if batch > self.__unittest_break__ and unittest:
                    break

    def test(self, dataset, unittest: bool = False) -> list:
        self.model.eval()

        epoch_start = time.time()
        self.metrics.init("validate")
        with torch.no_grad():
            batch_count = len(dataset)
            for j, batch in enumerate(dataset):
                # eval
                batch = self.to_recursive(batch, self.device)
                log = self.test_batch(
                    data=batch,
                    model=self.model,
                    optimizer=self.optimizer,
                    loss=self.loss,
                    batch=j,
                )
                self.detach_dict(log)
                self.metrics.collect("test", log)

                # display
                if (j % 10) == 0 or (j + 1) == batch_count:
                    progress = (j + 1) / batch_count
                    display_log = "[test] {} {} (remaining time: {} sec)".format(
                        tprint.bar(progress, width=40),
                        self.metrics.preview("test"),
                        tools.remaining_time(epoch_start, time.time(), progress),
                    )
                    tprint.print_line(display_log)

                if j > self.__unittest_break__ and unittest:
                    break

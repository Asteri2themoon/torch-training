import warnings


class GitRepoNotFoundWarning(UserWarning):
    pass


class CommitDontMatchWarning(UserWarning):
    pass


class ModuleDontMatchWarning(UserWarning):
    pass


class ClassDontMatchWarning(UserWarning):
    pass


class NotImplementedWarning(UserWarning):
    pass

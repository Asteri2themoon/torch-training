from . import tools

from .warnings import (
    GitRepoNotFoundWarning,
    CommitDontMatchWarning,
    ModuleDontMatchWarning,
    ClassDontMatchWarning,
    NotImplementedWarning,
)

import json
from typing import Type, Any
import copy
import warnings


class HParams(object):
    def __init__(self, **kwargs):
        self.__params__ = kwargs
        self.__expand__ = True

    def expand(self, exp: bool) -> None:
        self.__expand__ = exp

        for key, value in self.__params__.items():
            if type(value) == HParams:
                self.__params__[key].expand(exp)

    @property
    def params(self):
        return self.__params__

    def check_expand(self, attr: str) -> None:
        if (not (attr in self.__params__)) and (not self.__expand__):
            raise KeyError(f"attribute {attr} doesn't exist")

    def __getattr__(self, attr: str) -> Any:
        assert type(attr) == str

        if not (attr in self.__params__):
            self.__setattr__(attr, HParams())

        self.check_expand(attr)
        return self.__params__[attr]

    def __setattr__(self, attr: str, value: Any) -> None:
        assert type(attr) == str

        if attr[0] == "_":
            super(HParams, self).__setattr__(attr, value)
        else:
            self.check_expand(attr)
            self.__params__[attr] = value

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            result.__dict__[k] = copy.deepcopy(v, memo)
        return result

    def __merge_dict__(
        self, origin: Type["__class__"], target: Type["__class__"]
    ) -> None:
        assert type(origin) == self.__class__
        assert type(target) == self.__class__

        if target == {}:
            return
        for key, value in target.__params__.items():
            exist = key in origin.__params__

            if exist:
                current = origin.__params__[key]
                # cast type if different
                if exist and (type(current) != type(value)):
                    if type(current) == tuple and (type(value) in [int, float, str]):
                        # create tuple from scalar
                        value = tuple([value for _ in current])
                    else:
                        # cast value
                        value = type(current)(value)

                # cast inside tuple
                if type(current) == tuple:
                    # check length and types
                    if len(current) != len(value):
                        raise Exception("tuple should have the same size")

                    value = tuple([type(t)(v) for t, v in zip(current, value)])

            if exist and type(value) == self.__class__:
                self.__merge_dict__(origin.__params__[key], value)
            else:
                origin.__params__[key] = copy.deepcopy(value)

    def merge(self, hparams: Type["__class__"]) -> None:
        assert type(hparams) == self.__class__

        self.__merge_dict__(self, hparams)

    def __to_dict__(self) -> dict:
        result = copy.deepcopy(self.__params__)
        for key, value in result.items():
            if type(value) == HParams:
                result[key] = value.__to_dict__()
            else:
                result[key] = copy.deepcopy(value)
        return result

    @staticmethod
    def __from_dict__(dct: dict) -> Type["__class__"]:
        assert type(dct) == dict

        result = HParams()
        for key, value in dct.items():
            if type(value) == dict:
                result.__params__[key] = HParams.__from_dict__(value)
            else:
                result.__params__[key] = copy.deepcopy(value)
        return result

    def __str__(self) -> str:
        return json.dumps(self.__to_dict__(), indent=4, sort_keys=True)

    def items(self, reverse: bool = False) -> Any:
        assert type(reverse) == bool

        if not reverse:
            return self.__params__.items()
        else:
            result = {}
            for k in reversed(list(self.__params__.keys())):
                result[k] = self.__params__[k]
            return result.items()

    def has_scope(self, s: str):
        assert type(s) == str

        scope = s.split(".")[::-1]

        def get(hparams, scope):
            key = scope.pop()
            if not (key in hparams.__params__):
                return False
            if len(scope) > 0:
                return get(hparams.__getattr__(key), scope)
            else:
                return True

        return get(self, scope)

    def scope(self, s: str, expand: bool = True):
        assert type(s) == str

        class Scope:
            def __init__(self, hparams: HParams, s: str):
                assert type(hparams) == HParams
                assert type(s) == str

                self.hparams = hparams
                self.__scope__ = s

            def __enter__(self):
                scope = self.__scope__.split(".")[::-1]

                self.prev_expand_settings = self.hparams.__expand__
                if self.prev_expand_settings != expand:
                    self.hparams.expand(expand)

                def get(hparams, scope):
                    key = scope.pop()
                    if len(scope) > 0:
                        return get(hparams.__getattr__(key), scope)
                    else:
                        return hparams.__getattr__(key)

                return get(self.hparams, scope)

            def __exit__(self, type, value, traceback):
                if self.prev_expand_settings != expand:
                    self.hparams.expand(self.prev_expand_settings)

        return Scope(self, s)


class Settings:
    default = HParams()

    __start_commit__, _, _ = tools.get_current_git_state()
    if __start_commit__ is None:
        __start_commit__ = None

        warnings.warn(
            "Warning: git Repo not found, can't log commit number",
            GitRepoNotFoundWarning,
        )

    @staticmethod
    def set_default(default: HParams):
        assert type(default) == HParams

        Settings.default = default

    def __init__(self, hparams: HParams = HParams()):
        assert type(hparams) == HParams

        self.__commit__ = Settings.__start_commit__
        self.__hparams__ = copy.deepcopy(self.default)
        self.__hparams__.merge(hparams)

    @property
    def params(self):
        return self.__hparams__.params

    @property
    def commit(self) -> str:
        return self.__commit__

    def expand(self, exp: bool) -> None:
        self.__hparams__.expand(exp)

    def scope(self, s: str, expand: bool = True):
        assert type(s) == str

        return self.__hparams__.scope(s, expand)

    def has_scope(self, s: str):
        assert type(s) == str

        return self.__hparams__.scope(s)

    def __getattr__(self, attr: str) -> Any:
        assert type(attr) == str

        return self.__hparams__.__getattr__(attr)

    def __setattr__(self, attr: str, value: Any) -> None:
        assert type(attr) == str

        if attr[0] == "_":
            super(Settings, self).__setattr__(attr, value)
        else:
            self.__hparams__.__setattr__(attr, value)

    def __str__(self) -> str:
        return json.dumps(self.__hparams__.__to_dict__(), indent=4, sort_keys=True)

    def to_dict(self) -> dict:
        return self.__hparams__.__to_dict__()

    def from_dict(self, dct: dict) -> None:
        self.__hparams__ = HParams.__from_dict__(dct)

    @staticmethod
    def __flatten_dict__(dct: dict, root: str = "") -> dict:
        result = {}
        for k, v in dct.items():
            new_k = root + "/" + k if len(root) != 0 else k
            if type(v) == dict:
                tmp = Settings.__flatten_dict__(v, new_k)
                for k2, v2 in tmp.items():
                    result[k2] = v2
            else:
                result[new_k] = copy.deepcopy(v)
        return result

    def to_tensorboard_hparams(self) -> dict:
        import torch

        hparams = self.to_dict()
        hparams = self.__flatten_dict__(hparams)

        for k, v in hparams.items():
            if type(v) == list:  # only way to display it (sadly)
                hparams[k] = str(v)
            elif type(v) == torch.Tensor:  # cast torch Tensor
                if torch.prod(torch.tensor(v.shape)).item() == 1:
                    hparams[k] = v.item()
                else:
                    hparams[k] = str(v.tolist())
            elif not (type(v) in [bool, int, float, str]):  # try what you can!
                hparams[k] = str(v)

        return hparams

    def save(self, file_name: str, pretty: bool = True) -> None:
        assert type(file_name) == str
        assert type(pretty) == bool

        with open(file_name, "w") as fp:
            dct = self.__hparams__.__to_dict__()
            if self.__commit__ is not None:
                dct["__commit__"] = self.__commit__

            if pretty:
                json.dump(dct, fp, indent=4, sort_keys=True)
            else:
                json.dump(dct, fp)

    def load(self, file_name: str, fill_default=True) -> None:
        assert type(file_name) == str
        assert type(fill_default) == bool

        with open(file_name, "r") as fp:
            dct = json.load(fp)
            if "__commit__" in dct:
                self.__commit__ = dct["__commit__"]
                del dct["__commit__"]
            hparams = HParams.__from_dict__(dct)

        if fill_default:
            self.__hparams__ = copy.deepcopy(self.default)
            self.__hparams__.merge(hparams)
        else:
            self.__hparams__ = hparams

#!/usr/bin/env python
# -*- coding: utf-8 -*-


def init_class(name, path=None):
    if type(name) != str:
        raise Exception("Invalid class file of name")
    if path is not None and type(path) != str:
        raise Exception("Invalid path")

    import importlib
    import os
    import sys

    if path is not None:
        sys.path.insert(1, os.path.abspath(path))
    else:
        path = os.getcwd()

    file_name, class_name = name.split(".")
    file_absolute_path = os.path.join(path, file_name + ".py")

    spec = importlib.util.spec_from_file_location(file_name, file_absolute_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)

    return getattr(module, class_name)


def train(args):
    import torchtrain

    training_class = init_class(args.class_name, path=args.path)

    hparams = torchtrain.Settings()
    if args.hparams is not None:
        hparams.load(args.hparams)

    training = training_class(
        dataset_path=args.dataset,
        from_checkpoint=args.from_checkpoint,
        to_checkpoint=args.checkpoint,
        metrics_output=args.metrics,
        device=args.device,
        num_workers=args.workers,
        hparams=hparams,
        logs=args.logs,
        iter_train=args.iter_train,
        iter_validate=args.iter_validate,
        iter_train_total=args.iter_train_total,
        interval_backup=args.interval_backup,
    )
    training.run()


def unittest(args):
    import torchtrain

    training_class = init_class(args.class_name, path=args.path)

    hparams = torchtrain.Settings()
    if args.hparams is not None:
        hparams.load(args.hparams)

    training = training_class(
        dataset_path=args.dataset,
        from_checkpoint=None,
        to_checkpoint=args.checkpoint,
        metrics_output=args.metrics,
        device=args.device,
        num_workers=args.workers,
        hparams=hparams,
        logs=args.unittest,
        iter_train=args.iter_train,
        iter_validate=args.iter_validate,
        iter_train_total=args.iter_train_total,
        interval_backup=args.interval_backup,
    )
    if not training.unittest():
        print("No error")


def default(args):
    import torchtrain

    init_class(args.class_name, path=args.path)

    hparams = torchtrain.Settings()
    hparams.save(args.hparams)


def config_file(args):
    args.save_default()


def interactive(args):
    import torchtrain

    training_class = init_class(args.class_name, path=args.path)

    hparams = torchtrain.Settings()
    if args.hparams is not None:
        hparams.load(args.hparams)

    training = training_class(
        dataset_path=args.dataset,
        from_checkpoint=args.from_checkpoint,
        to_checkpoint=args.checkpoint,
        device=args.device,
        metrics_output=args.metrics,
        num_workers=args.workers,
        hparams=hparams,
        logs=args.logs,
        iter_train=args.iter_train,
        iter_validate=args.iter_validate,
        iter_train_total=args.iter_train_total,
        interval_backup=args.interval_backup,
    )
    breakpoint()


def main():
    import argparse

    from .config import Config

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    train_parser = subparsers.add_parser("train", help="Run torchtrain.Training")
    train_parser.set_defaults(which="train")

    Config.setup_parser(
        train_parser,
        training_class=True,
        dataset=True,
        workers=True,
        from_checkpoint=True,
        checkpoint=True,
        metrics=True,
        device=True,
        hparams=True,
        logs=True,
        unittest=False,
        path=True,
        iter_train=True,
        iter_validate=True,
        iter_train_total=True,
        interval_backup=True,
    )

    unittest_parser = subparsers.add_parser(
        "unittest", help="Check torchtrain.Training"
    )
    unittest_parser.set_defaults(which="unittest")

    Config.setup_parser(
        unittest_parser,
        training_class=True,
        dataset=True,
        workers=True,
        from_checkpoint=False,
        checkpoint=True,
        metrics=True,
        device=True,
        hparams=True,
        logs=False,
        unittest=True,
        path=True,
        iter_train=True,
        iter_validate=True,
        iter_train_total=True,
        interval_backup=True,
    )

    default_parser = subparsers.add_parser(
        "default", help="generate default config from torchtrain.Training"
    )
    default_parser.set_defaults(which="default")

    Config.setup_parser(
        default_parser,
        training_class=True,
        dataset=False,
        workers=False,
        from_checkpoint=False,
        checkpoint=False,
        metrics=False,
        device=False,
        hparams=True,
        logs=False,
        unittest=False,
        path=True,
        iter_train=False,
        iter_validate=False,
        iter_train_total=False,
        interval_backup=False,
    )

    interactive_parser = subparsers.add_parser(
        "interactive", help="restart from a interactive with a debugger"
    )
    interactive_parser.set_defaults(which="interactive")

    Config.setup_parser(
        interactive_parser,
        training_class=True,
        dataset=True,
        workers=True,
        from_checkpoint=True,
        checkpoint=True,
        metrics=True,
        device=True,
        hparams=True,
        logs=True,
        unittest=False,
        path=True,
        iter_train=True,
        iter_validate=True,
        iter_train_total=True,
        interval_backup=True,
    )

    config_parser = subparsers.add_parser(
        "config", help="generate a configuration file"
    )
    config_parser.set_defaults(which="config")

    Config.setup_parser(
        config_parser,
        training_class=True,
        dataset=True,
        workers=True,
        from_checkpoint=True,
        checkpoint=True,
        metrics=True,
        device=True,
        hparams=True,
        logs=True,
        unittest=True,
        path=True,
        iter_train=True,
        iter_validate=True,
        iter_train_total=True,
        interval_backup=True,
    )

    args = parser.parse_args()

    conf = Config(args)

    if args.which == "train":
        train(conf)
    elif args.which == "unittest":
        unittest(conf)
    elif args.which == "default":
        default(conf)
    elif args.which == "interactive":
        interactive(conf)
    elif args.which == "config":
        config_file(conf)
    else:
        raise Exception(f"subparsers {args.which} not found!")


if __name__ == "__main__":
    main()

import json
import copy


class Config:
    default_config = {
        "class_name": "file_name.class_name",
        "path": ".",
        "dataset": "/path/to/dataset/directory",
        "workers": 8,
        "device": "cpu",
        "logs": "runs",
        "unittest": "unittest",
        "checkpoint": "checkpoint.pth",
        "metrics": "metrics.json",
        "hparams": "hparams.json",
        "iter_train": 1000,
        "iter_validate": 100,
        "iter_train_total": 100000,
        "interval_backup": 600,
    }
    if_no_args = {
        "path": ".",
        "workers": 8,
        "device": "cpu",
        "iter_train": 1000,
        "iter_validate": 100,
        "iter_train_total": 100000,
        "interval_backup": 600,
    }

    def __init__(self, args):
        self.config = vars(args)

        try:
            with open(self.config.get("config", ""), "r") as f:
                from_config = json.load(f)
        except FileNotFoundError:
            pass
        else:
            self._merge(self.config, from_config, replace=False)

        self._merge(self.config, self.if_no_args, replace=False)

    def __getattribute__(self, key: str):
        if key in ["default_config", "config", "if_no_args"]:
            return object.__getattribute__(self, key)
        elif (
            key in {**self.default_config, "from_checkpoint": None}
            and key in self.config
        ):
            return self.config.get(key, None)
        # else:
        #    raise KeyError("key attributes doesn't exist")
        else:
            return object.__getattribute__(self, key)

    def save_default(self, file_name: str = None) -> None:
        if file_name is None:
            if "config" in self.config:
                file_name = self.config["config"]
            else:
                raise Exception(
                    "You should setup the configuration file name of your Config object or pass save_default to set save_default function in order to save default configuration"
                )

        saved_config = copy.deepcopy(self.config)

        self._merge(saved_config, self.default_config, replace=False)
        self._filter(saved_config)

        with open(file_name, "w") as f:
            json.dump(saved_config, f, indent=4, sort_keys=True)

    def __str__(self):
        return json.dumps(self.config, indent=4, sort_keys=True)

    @staticmethod
    def _merge(dst: dict, src: dict, replace: bool = False) -> None:
        for key, value in src.items():
            if dst.get(key, None) == None or replace:
                dst[key] = value

    @staticmethod
    def _filter(src: dict) -> None:
        for key in list(src.keys()):
            if key not in Config.default_config:
                del src[key]

    @staticmethod
    def setup_parser(
        parser,
        config: bool = True,
        training_class: bool = True,
        dataset: bool = False,
        workers: bool = False,
        from_checkpoint: bool = False,
        checkpoint: bool = False,
        metrics: bool = False,
        device: bool = False,
        hparams: bool = False,
        logs: bool = False,
        unittest: bool = False,
        path: bool = False,
        iter_train: bool = False,
        iter_validate: bool = False,
        iter_train_total: bool = False,
        interval_backup: bool = False,
    ) -> None:
        if config:
            parser.add_argument(
                "config",
                nargs="?",
                default="config.json",
                help="configuration file (json format)",
            )
        if training_class:
            parser.add_argument(
                "--class-name",
                "-c",
                default=None,
                help="class to train, specify the class with the following syntax: file_name.class_name",
            )
        if dataset:
            parser.add_argument(
                "--dataset", "-d", default=None, help="path to the dataset directory"
            )
        if workers:
            parser.add_argument(
                "--workers",
                "-w",
                type=int,
                default=None,
                help="path to the dataset directory",
            )
        if from_checkpoint:
            parser.add_argument(
                "--from-checkpoint",
                "-f",
                default=None,
                help="Start training from a checkpoint",
            )
        if checkpoint:
            parser.add_argument(
                "--checkpoint", "-C", default=None, help="output checkpoint",
            )
        if metrics:
            parser.add_argument(
                "--metrics",
                "-m",
                default=None,
                help="output file to save metrics (json format)",
            )
        if device:
            parser.add_argument(
                "--device", "-D", default=None, help="device to run the training",
            )
        if hparams:
            parser.add_argument(
                "--hparams",
                "-H",
                default=None,
                help="hyperparameters configuration, path to the file or json format",
            )
        if logs:
            parser.add_argument("--logs", "-l", default=None, help="logs directory")
        if unittest:
            parser.add_argument(
                "--unittest", "-u", default=None, help="logs directory for unit test"
            )
        if path:
            parser.add_argument(
                "--path",
                "-p",
                default=None,
                help="append to path, can be setup with the path of the file containing the Training class (fix import issues)",
            )
        if iter_train:
            parser.add_argument(
                "--iter-train",
                "-t",
                type=int,
                default=None,
                help="number of training iteration between two validation",
            )
        if iter_validate:
            parser.add_argument(
                "--iter-validate",
                "-v",
                type=int,
                default=None,
                help="number of training iteration between two training",
            )
        if iter_train_total:
            parser.add_argument(
                "--iter-train-total",
                "-T",
                type=int,
                default=None,
                help="Total number of training iteration",
            )
        if interval_backup:
            parser.add_argument(
                "--interval-backup",
                "-b",
                type=float,
                default=None,
                help="Invervale between two backup in seconds",
            )

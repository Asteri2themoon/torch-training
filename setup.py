import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="torch-train",  # Replace with your own username
    version="0.0.3",
    author="Arthur Klipfel",
    author_email="arthur.klipfel1@gmail.com",
    description="Framework to train deep model with pytorch easily",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Clodion/torch-training",
    packages=setuptools.find_packages(),
    install_requires=["torch", "torchvision", "pandas", "numpy", "pygit2"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={"console_scripts": ["torch-train=torchtrain.command_line:main"],},
    python_requires=">=3.7",
)
